declare module '*/projectQuery.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const projectQuery: DocumentNode;

  export default defaultDocument;
}

declare module '*/projectsQuery.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const projectsQuery: DocumentNode;

  export default defaultDocument;
}
