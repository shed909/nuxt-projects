import express from 'express';
import bodyParser from 'body-parser';
import axios from 'axios';

const SECRET_KEY = process.env.CAPTCHA_SECRET_KEY || '';

const app = express();

app.use(bodyParser.json());

app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);

app.post('/hcaptcha-verify', async (req, res) => {
  try {
    const token = req.body.token;

    if (!token) {
      res.status(400).send('No token!');
      throw 'No token!';
    }

    const params = new URLSearchParams();
    params.append('secret', SECRET_KEY);
    params.append('response', token);

    const { data } = await axios.post(
      `https://hcaptcha.com/siteverify`,
      params,
    );

    res.send(data);
  } catch (e) {
    console.log('hCAPTCHA error:', e);
    res.status(500).send(`<p>hCAPTCHA error:</p> <pre>${e}</pre>`);
  }
});

export default app;
