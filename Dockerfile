FROM node:lts-alpine3.15@sha256:bb776153f81d6e931211e3cadd7eef92c811e7086993b685d1f40242d486b9bb

WORKDIR /usr/src/app

COPY ./package.json ./yarn.lock ./.nvmrc ./

RUN yarn

COPY --chown=node:node . .

ENV NODE_ENV production

RUN yarn build && \
    yarn cache clean

EXPOSE 3000

USER node
CMD [ "yarn", "start" ]
