module.exports = {
  mode: 'jit',
  darkMode: 'class',
  theme: {
    fontFamily: {
      ninito: ['Nunito'],
      comfortaa: ['Comfortaa'],
    },
    extend: {
      invert: {
        25: '.25',
        50: '.5',
        30: '.3',
        75: '.75',
      },
      colors: {
        color: '#243746',
        'color-primary': '#1f6f62',
        'color-secondary': '#0e2233',
        background: '#fefcfa',
        'background-secondary': '#e3e0da',
        'background-code': '#d2d5da',
        'code-inline': '#353538',
        'border-color': '#ddd',
        // Dark mode
        'color-dark': '#ebf4f1',
        'color-primary-dark': '#34a47b',
        'color-secondary-dark': '#fdf9f3',
        'background-dark': '#091a28',
        'background-secondary-dark': '#071521',
        'background-code-dark': '#3d414a',
        'code-inline-dark': '#dedede',
        'border-color-dark': '#0d2538',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  content: [
    `components/**/*.{vue,js}`,
    `layouts/**/*.vue`,
    `pages/**/*.vue`,
    `plugins/**/*.{js,ts}`,
    `nuxt.config.{js,ts}`,
  ],
};
