# Info

This is a simple Nuxt.JS portfolio website built to replace a quick and dirty [Flask website](https://gitlab.com/shanehull/flask-portfolio) I built a long time ago.

It uses the [GitLab Projects API](https://docs.gitlab.com/ee/api/projects.html) GraphQL endpoint to populates cards on the "projects" section of the site, displaying the avatar, title and description.

Any project card can be clicked, taking you to a "project" page that displays information about the project along with the fully formatted `README.md`.

It supports GitLab flavoured markdown TOC's, tables, code highlighting and LaTeX, so the projects README.md can also take the form of a blog article.

If you're not reading this on the website, you can check it out [here](https://shanehull.com).

# Populating projects

The GraphQL query used to retrieve projects searches all public projects in the specified namespace and filters by "topics".

To add a project to the website, it just needs to be tagged with the same value specified in the .env file:

```bash
PROJECT_TAG=shanehull.com
GITLAB_NAMESPACE=shanehull
```
