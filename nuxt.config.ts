import hljs from 'highlight.js';
import axios from 'axios';

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Shane Hull',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1.0, user-scalable=no',
      },
      {
        hid: 'description',
        name: 'description',
        content: "Shane Hull's corner of the intertubes.",
      },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  loading: { color: 'var(--bg)' },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/css/main.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: '~plugins/vue-hcaptcha', ssr: false }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/tailwindcss',
    '@nuxt/postcss8',
    '@nuxtjs/color-mode',
    '@nuxtjs/svg',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'nuxt-helmet',
    '@nuxtjs/apollo',
    '@nuxtjs/axios',
    '@nuxtjs/markdownit',
    '@nuxtjs/gtm',
    '@nuxtjs/sitemap',
    '@nuxtjs/google-fonts',
    [
      'nuxt-mail',
      {
        message: {
          to: process.env.MAIL_TO,
        },
        smtp: {
          host: process.env.MAIL_HOST,
          port: process.env.MAIL_PORT,
          auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASS,
          },
        },
      },
    ],
  ],
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // extractCSS: true,
    postcss: {
      plugins: {
        'tailwindcss/nesting': {},
        tailwindcss: {},
        autoprefixer: {},
      },
    },
  },

  publicRuntimeConfig: {
    siteName: process.env.SITE_NAME,
    siteKey:
      process.env.CAPTCHA_SITE_KEY || '10000000-ffff-ffff-ffff-000000000001',
  },
  privateRuntimeConfig: {},

  serverMiddleware: ['~/api/hcaptcha.ts'],

  tailwindcss: {
    exposeConfig: true,
  },

  // apollo config
  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint: 'https://gitlab.com/api/graphql',
      },
    },
    defaultOptions: {
      // See 'apollo' definition
      // For example: default query options
      $query: {
        loadingKey: 'loading',
        fetchPolicy: 'cache-and-network',
      },
    },
  },

  axios: {
    baseURL: process.env.BASE_URL || 'http://localhost:3000',
  },

  // markdownit config
  markdownit: {
    preset: 'default',
    runtime: true,
    linkify: true,
    html: false,
    breaks: true,
    injected: true,
    typographer: true,
    use: [
      'markdown-it-anchor',
      'markdown-it-toc-done-right',
      'markdown-it-highlightjs',
      [
        'markdown-it-texmath',
        {
          delimiters: ['gitlab'],
          katexOptions: { strict: false },
        },
      ],
    ],
    // use syntax highlighting
    highlight: function (str: string, lang: string) {
      if (lang && hljs.getLanguage(lang)) {
        try {
          return hljs.highlight(lang, str).value;
        } catch (__) {}
        return ''; // use external default escaping
      }
    },
  },

  googleFonts: {
    families: {
      Comfortaa: {
        wght: [400, 600],
      },
      Nunito: {
        wght: [200, 400, 600],
      },
    },
    display: 'swap',
    prefetch: false,
    preconnect: false,
    preload: false,
    download: true,
    inject: true,
    base64: false,
  },

  colorMode: {
    classSuffix: '',
  },

  gtm: {
    id: process.env.GOOGLE_TAG_MANAGER_ID || 'GTM-5JQFQ57',
    scriptDefer: true,
    pageTracking: true,
    enabled: true,
  },

  sitemap: {
    routes: async () => {
      const data = await axios
        .get(
          `https://gitlab.com/api/v4/projects?search_namespaces=true&search=${
            process.env.GITLAB_NAMESPACE || 'shanehull'
          }&topic=${[process.env.PROJECT_TAG || 'shanehull.com']}`,
        )
        .then((data) =>
          data.data.map((proj: { path: string }) => `/projects/${proj.path}`),
        );

      return data;
    },
  },
};
